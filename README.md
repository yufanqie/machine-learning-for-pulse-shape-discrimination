# Machine Learning for Pulse Shape Discrimination



## Data Preparation

There are different choices of inputs:
1) Summed S1 pulse (Summing raw POD from each channel, the entire waveform, not strictly in [PulseStart, PulseEnd]. Since pulseStartTime in LZap is defined as the time when 5% area is achieved, ignoring the first 5% area will lose some information about PSD. The raw POD is inverted and baseline subtracted. Baseline is defined by first 25 samples and last 25 samples.)

2) For each S1, PODs from individual PMT channel, and its start time. Shape = (sample_size, num_POD,POD_length) -> For this one, the dimension of the input sample is too large and may cause overfitting. So, this type of input will not be tried at first.

3) Photon timings of the S1 pulse obtained from n-ph model fitting. For each S1, the reference time is the medium. I am worried that this will lose some information relating to the properties of S1 pulse such as S1 pulse area, S1 pulse height or so. I will consider combining some additional features besides the photon timings from template fitting. 

4) Besides 1)2)3), RQs can be added as additional inputs. For example, PSD is depth-dependent, so it's important to pass this information to the ML models. In addition, some other RQs like channel ID, pulseStartTime.. may not seem useful at first but may be important. The useness of these RQs can be evaluated by comparing performance of models with and without them. We can try passing RQs with waveforms as inputs by using feature concatenation or lowering the dimension by using PCA or autoencoder. I am personally a little worried that adding RQs at first may influence the analysis of the model to waveforms. For example, for CNN, the location of the features are important. For NN, it's easily overfit since the training data size is not very large.(if without simulated data) Therefore, I would try applying encoder-decoder. Encoder is used to process input like raw waveforms or photon timings and decoder is used to combine information between encoder and RQs.

5) For simulated data, it can only be S1 pulse or S1 photon timings. There is no information about raw POD from individual PMT channel. I will check whether there are other RQs which can be obtained from the simulation of NEST.

6) Later, I will consider adding WS data as unsupervised learning, and combine it with supervised learning. 

Note: For real data from SR1 CH3T, DD and WS dataset, the S1 pulses used in training & testing ML models are the same pulses used in pulse shape discrimination study. 

## Significance
* Various size of input samples: For inputs like S1 pulse or S1 photon timings, the dimension of each sample is different. For most ML models (except RNN and Transformer), the dimension of the input sample is required to be the same. So, padding is required. 
* Cost-sensitive loss: In the problem of pulse shape discrimination, we care more about ER leakage. This means that we focus more on whether ER events are classified as NR events. For models with the same total accuracy, we prefer the one with more accuracy in classifying ER events. So, we will consider using cost-sensitive loss functions, which has different weights for different misclassifying classes. 

## Models
Logistic Regression -> Support Vector Machine -> Multi-layer Perceptron -> Neural Network -> Convolutional Neural Network -> Transformer

For all models, label NR events as 1 and ER events as 0. The data set is divided into 75% training data and 25% testing data. Random shuffle is applied when dividing. 

For SR1, after the data selection, the number of ER S1 pulses + NR S1 pulses is 11335. 
